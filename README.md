# Traffic Light Example
This example demonstrates the implementation of the Lighting application using dynamic UI, the MVCC (Model View Controller Component) pattern and serialization in JSON.

**Developer:** Ilya Rastorguev
**Unity Version:** 2020.1.5f1

You can download **Unitypackage** from folder **/Unitypackage/** folder and use it in your project.