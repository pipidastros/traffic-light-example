﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Traffic Light View
/// </summary>
public class TrafficLightView : BaseView{
    [Header("View Parts")]
    public Color defaultLight;                                      // Disabled Light
    public GameObject Container;                                    // Container
    public GameObject Block;                                        // Block

    /* Private Params */
    private bool isInitialized = false;                             // Initialization Status
    private List<GameObject> Blocks = new List<GameObject>();       // Current Game objects
    private float TimeToSwitch = 0F;                                // Time To Switch

    /* Model Events */
    public delegate void TrafficLightChanged();
    public event TrafficLightChanged OnLightChanged;

    /// <summary>
    /// Update Every Frame if Initialized
    /// </summary>
    private void Update(){
        if (isInitialized) _UpdateColors();
    }

    /// <summary>
    /// Update Light Colors
    /// </summary>
    protected void _UpdateColors(){
        if(TimeToSwitch <= 0F){
            SwitchNextColor();
        }else{
            TimeToSwitch -= Time.deltaTime;
        }
    }

    /// <summary>
    /// Switch to Color by Light Index
    /// </summary>
    /// <param name="lightBlock">Light Block Index</param>
    public void SwitchToColor(int lightBlock){
        // Setup Current
        Blocks[this.app.TLModel.currentLight].transform.Find("Light").transform.gameObject.GetComponent<Image>().color = defaultLight;

        // Setup Active
        this.app.TLModel.currentLight = lightBlock;
        Blocks[lightBlock].transform.Find("Light").transform.gameObject.GetComponent<Image>().color = this.app.TLModel.lightBlocks[lightBlock].lightColor;
        this.app.TLModel.SaveData("traffic_light.dat", this.app.TLModel);
        TimeToSwitch = this.app.TLModel.lightBlocks[lightBlock].timeToChange;

        /* Call Event */
        if (OnLightChanged != null) OnLightChanged();
    }

    /// <summary>
    /// Switch to the Next Color
    /// </summary>
    public void SwitchNextColor(){
        int _next = ((this.app.TLModel.currentLight + 1)>this.app.TLModel.lightBlocks.Count - 1)?0: this.app.TLModel.currentLight + 1;
        SwitchToColor(_next);
    }

    /// <summary>
    /// Update Traffic Light View
    /// </summary>
    public void UpdateView(){
        for(int i = 0; i < this.app.TLModel.lightBlocks.Count; i++){
            AddBlock();
        }

        // Switch Color
        SwitchToColor(this.app.TLModel.currentLight);
        isInitialized = true;
    }

    /// <summary>
    /// Clear View
    /// </summary>
    public void ClearView(){
        for(int i = 0; i < Blocks.Count; i++){
            GameObject.Destroy(Blocks[i]);
            Blocks.RemoveAt(i);
        }

        this.app.TLModel.SaveData("traffic_light.dat", this.app.TLModel);
    }

    /// <summary>
    /// Add Block
    /// </summary>
    public void AddBlock(){
        GameObject _newBlock = GameObject.Instantiate(Block, Container.transform);
        Blocks.Add(_newBlock);
        this.app.TLModel.SaveData("traffic_light.dat", this.app.TLModel);
    }
}
