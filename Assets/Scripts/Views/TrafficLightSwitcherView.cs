﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Traffic Light Switcher View
/// </summary>
public class TrafficLightSwitcherView : BaseView{
    private Button _Button;

    /// <summary>
    /// Before Scene Started Handler
    /// </summary>
    private void Awake(){
        _Button = this.gameObject.GetComponent<Button>();
    }

    /// <summary>
    /// Add Switcher Handler
    /// </summary>
    public void AddSwitcherHandler(){
        _Button.onClick.AddListener(()=>{
            this.app.TLSwitcherController.SwitchLight();
        });
    }
}
