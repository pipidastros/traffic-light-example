﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Traffic Light Status Text View
/// </summary>
public class StatusTextView : MonoBehaviour{
    protected bool _isInitialized = false;      // Initialization Flag
    protected Text _textView;                   // Text UI

    /// <summary>
    /// Awake Method
    /// </summary>
    private void Awake(){
        _textView = this.gameObject.GetComponent<Text>();
        _isInitialized = true;
    }

    /// <summary>
    /// Change Text
    /// </summary>
    /// <param name="text">Text to Change</param>
    public void ChangeText(string text){
        _textView.text = text;
    }
}
