﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Traffic Light Model (Serializable)
/// </summary>
[System.Serializable]
public class TrafficLightModel : BaseModel{
    public bool isEnabled = true;                                   // Traffic Light Enabled?
    public int currentLight = 0;                                    // Traffic Light Current
    public List<lightBlock> lightBlocks = new List<lightBlock>();   // Traffic Lights Data

    [System.Serializable]
    public class lightBlock{
        public string name = "";            // Name of the Light
        public float timeToChange = 2F;     // Time to Switch to Another Light
        public Color lightColor;            // Light Color
    }
}
