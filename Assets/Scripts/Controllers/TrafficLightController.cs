﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Traffic Light Controller
/// </summary>
public class TrafficLightController : BaseController{
    /// <summary>
    /// Get Current Color Name
    /// </summary>
    /// <returns>Color Name</returns>
    public string GetCurrentColorName(){
        int _current = this.app.TLModel.currentLight;
        return this.app.TLModel.lightBlocks[_current].name;
    }

    /// <summary>
    /// Get Current Color ID
    /// </summary>
    /// <returns>Color ID</returns>
    public int GetCurrentColorID(){
        int _current = this.app.TLModel.currentLight;
        return _current;
    }

    /// <summary>
    /// Get Current Color
    /// </summary>
    /// <returns>Color</returns>
    public Color GetCurrentColor(){
        int _current = this.app.TLModel.currentLight;
        return this.app.TLModel.lightBlocks[_current].lightColor;
    }

    /// <summary>
    /// Switch to Next Color
    /// </summary>
    public void SwitchNextColor(){
        this.app.TLView.SwitchNextColor();
    }

    /// <summary>
    /// Switch color by Light ID
    /// </summary>
    /// <param name="lightID">ID</param>
    public void SwitchNextColorByID(int lightID){
        this.app.TLView.SwitchToColor(lightID);
    }
}
