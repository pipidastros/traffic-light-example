﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Traffic Light Switcher
/// </summary>
public class TrafficLightSwitcher : BaseController{
    /// <summary>
    /// Switch Light to Next
    /// </summary>
    public void SwitchLight(){
        this.app.TLController.SwitchNextColor();
    }
}
