﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine;

/// <summary>
/// Storage Component Class
/// This component allows you to work with file storage.
/// </summary>
public class StorageComponent : BaseElement{
    /* Component Params */
    [Header("General Settings")]
    [Tooltip("Please, specify folder to store data")]
    public string pathToStoreFiles = "/";               // Path to Store Files

    /// <summary>
    /// Initialize Settings Before scene Start
    /// </summary>
    private void Awake(){
        /* Set Path to Store Files on Device */
        pathToStoreFiles = Application.persistentDataPath + pathToStoreFiles;
    }

    /// <summary>
    /// Save Data to File
    /// </summary>
    /// <param name="fileName">File Name</param>
    /// <param name="fileData">File Data</param>
    /// <param name="isEncoded">Encode file using Base64?</param>
    public void SaveFile(string fileName, string fileData = "", bool isEncoded = false){
        /* Encode Base64 File */
        if (isEncoded){
            byte[] _bytesToEncode = Encoding.UTF8.GetBytes(fileData);
            fileData = Convert.ToBase64String(_bytesToEncode);
        }

        /* Save File to Path */
        File.WriteAllText(pathToStoreFiles + fileName, fileData, Encoding.UTF8);
        Debug.Log(String.Format("File \"{0}\" successfully saved to \"{1}\"", fileName, pathToStoreFiles));
    }

    /// <summary>
    /// Load Data from File
    /// </summary>
    /// <param name="fileName">File Name</param>
    /// <param name="isEncoded">Decode file from Base64?</param>
    /// <returns>Decoded data from File</returns>
    public string LoadFile(string fileName, bool isEncoded = false){
        /* Trying to find File */
        if (!File.Exists(pathToStoreFiles + fileName)){
            Debug.Log(String.Format("Failed to load file \"{0}\". File not found in \"{1}\"", fileName, pathToStoreFiles));
            return null;
        }

        /* Load File */
        string _data = File.ReadAllText(pathToStoreFiles + fileName);
        if (isEncoded){
            byte[] _decodedBytes = Convert.FromBase64String(_data);
            string _decodedData = Encoding.UTF8.GetString(_decodedBytes);
            return _decodedData;
        }

        // Return Data
        Debug.Log(String.Format("File \"{0}\" successfully loaded from \"{1}\"", fileName, pathToStoreFiles));
        return _data;
    }
}
