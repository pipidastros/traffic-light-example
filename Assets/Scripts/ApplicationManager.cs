﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Application Manager Class
/// </summary>
public class ApplicationManager : MonoBehaviour {
    [Header("Controllers")]
    [Tooltip("Place here link to Traffic Light Controller")]
    public TrafficLightController TLController;
    public TrafficLightSwitcher TLSwitcherController;

    [Header("Models")]
    [Tooltip("Place here link to Traffic Light Model")]
    public TrafficLightModel TLModel;

    [Header("Views")]
    [Tooltip("Place here link to Traffic Light View")]
    public TrafficLightView TLView;
    public StatusTextView TLStatusView;
    public TrafficLightSwitcherView TLSwitcherView;

    [Header("Components")]
    [Tooltip("Place here link to Storage Component")]
    public StorageComponent Storage;

    /// <summary>
    /// Use Before scene started
    /// </summary>
    private void Awake(){
        /* Load Traffic Light Data*/
        TLModel.LoadData("traffic_light.dat", TLModel);
        TLView.OnLightChanged += OnTrafficLightChanedHandler;
        TLView.UpdateView();
    }

    /// <summary>
    /// Start
    /// </summary>
    private void Start(){
        /* Add Handlers */
        TLSwitcherView.AddSwitcherHandler();
    }

    /// <summary>
    /// Handler for Traffic Light Changed Event
    /// </summary>
    private void OnTrafficLightChanedHandler(){
        TLStatusView.ChangeText(string.Format("Current Light: {0}, #{1}", TLController.GetCurrentColorName(), TLController.GetCurrentColorID().ToString("N0")));
    }
}
