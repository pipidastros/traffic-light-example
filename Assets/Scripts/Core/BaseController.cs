﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Base Controller Class
/// The basic class of the controller used for inheritance in all other elements.
/// </summary>
public class BaseController : BaseElement{
    
}
