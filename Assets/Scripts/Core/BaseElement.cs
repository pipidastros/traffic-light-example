﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * Base Application Element
 * This class is a framework for models, controllers, views and components. 
 * It allows easy access to other elements of the system.
 */
public class BaseElement : MonoBehaviour{
    /* Application Manager Instance */
    public ApplicationManager app { get { return GameObject.FindObjectOfType<ApplicationManager>(); } }
}
