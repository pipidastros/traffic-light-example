﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

/// <summary>
/// Base Model Class
/// The basic class of the model used for inheritance in all other elements.
/// </summary>
public class BaseModel : BaseElement{
    /// <summary>
    /// Save Data of Object to JSON file
    /// </summary>
    /// <param name="filename">Filename</param>
    /// <param name="dataToSave">Object to Save</param>
    public void SaveData(string filename, object dataToSave){
        string _data = JsonUtility.ToJson(dataToSave);
        this.app.Storage.SaveFile(filename, _data, true);
    }

    /// <summary>
    /// Load Data from JSON
    /// </summary>
    /// <param name="filename">Filename</param>
    /// <param name="Reference">Target Object</param>
    /// <returns>Overriten Object</returns>
    public object LoadData(string filename, object Reference){
        string _data = this.app.Storage.LoadFile(filename, true);
        if (_data != null){
            JsonUtility.FromJsonOverwrite(_data, Reference);
        }
        
        return Reference;
    }
}