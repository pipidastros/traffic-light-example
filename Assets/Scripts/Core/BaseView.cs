﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Base View Class
/// The basic class of the view used for inheritance in all other elements.
/// </summary>
public class BaseView : BaseElement{
    
}
